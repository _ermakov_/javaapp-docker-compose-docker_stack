#Build service
```
docker build -t test -f apps/api/Dockerfile .
```
#Start service
```
docker-compose up -d
```
#Start service through dockerstack
```
docker stack deploy -c stack.yaml app
docker service ls
docker stack rm app
```

